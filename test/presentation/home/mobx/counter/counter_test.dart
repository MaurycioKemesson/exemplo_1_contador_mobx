import 'package:exemplo_1_contador_mobx/app/presentation/home/mobx/counter/counter.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    'Teste para quando estiver 0 e o usuário clicar no botão adicionar'
    'devera retornar 1',
    () {
      final _counterController = CounterController();
      expect(_counterController.count, 0);
      _counterController.add();
      expect(_counterController.count, 1);
    },
  );

  test(
    'Teste para quando estiver 0 e o usuário clicar no botão diminuir'
    'devera retornar -1',
    () {
      final _counterController = CounterController();
      expect(_counterController.count, 0);
      _counterController.sub();
      expect(_counterController.count, -1);
    },
  );
}

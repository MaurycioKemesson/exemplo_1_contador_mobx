import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';
import 'package:exemplo_1_contador_mobx/app/presentation/shopping/mobx/shopping/shopping.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    'Adicionando um item a lista'
    'e verificando se esse item foi adicionado',
    () {
      final todoList = ShoppingController();
      final item = ItemEnteties(name: 'Item adicionado');
      todoList.addItem(item);
      expect(todoList.items.length, 1);
      expect(todoList.items.contains(item), true);
    },
  );

  test(
      'Removendo um item da lista'
      'e verificando se o item realmente foi removido', () {

    final todoList = ShoppingController();
    final item = ItemEnteties(name: 'Item adicionado');
    todoList.addItem(item);

    expect(todoList.items.length, 1);
    expect(todoList.items.contains(item), true);

    todoList.removeItem(0);

    expect(todoList.items.length, 0);
    expect(todoList.items.contains(item), false);
  });
}

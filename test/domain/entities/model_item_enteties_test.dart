import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  late ItemEnteties item;

  setUp(
    () async {
      item = ItemEnteties(name: 'Item 1');
    },
  );

  test('Deve verificar se o nome do item possui 6 caracteres', () {
    String name = item.name;
    expect(name.length, equals(6));
  });
}

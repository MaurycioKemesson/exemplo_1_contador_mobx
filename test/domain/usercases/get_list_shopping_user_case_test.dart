import 'package:dartz/dartz.dart';
import 'package:exemplo_1_contador_mobx/app/core/erros/generic_exception.dart';
import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';
import 'package:exemplo_1_contador_mobx/app/domain/repositories/shopping_repository.dart';
import 'package:exemplo_1_contador_mobx/app/domain/usercases/imp/shopping_use_case_imp.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class ShoppingRepositoryMock extends Mock implements ShoppingRepository {}

void main() {
  late ShoppingRepositoryMock shoppingRepository;
  late ShoppingUseCaseImp shoppingUseCaseImp;
  late List<ItemEnteties> listModelItem;

  setUp(() async {
    shoppingRepository = ShoppingRepositoryMock();
    shoppingUseCaseImp = ShoppingUseCaseImp(shoppingRepository);
    listModelItem = <ItemEnteties>[];
  });

  test('Deve retornar a lista de itens', () async {
    when(() => shoppingRepository.getList()).thenAnswer((_) async {
      return Future.value(Right(listModelItem));
    });

    var result = await shoppingUseCaseImp.getList();

    verify(() => shoppingRepository.getList());

    expect(result.isRight(), true);
  });

  test('Deve retornar um erro ao tentar listar itens', () async {
    when(() => shoppingRepository.getList()).thenAnswer((_) async {
      return Future.value(Left(GenericException()));
    });

    var result = await shoppingUseCaseImp.getList();

    verify(() => shoppingRepository.getList());

    expect(result.isLeft(), true);
  });
}

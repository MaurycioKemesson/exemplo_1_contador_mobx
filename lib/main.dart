import 'package:exemplo_1_contador_mobx/app/presentation/home/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Exemplo 1 Contador Mobx Teste',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const HomePage(title: 'Exemplo 1 Contador Mobx Teste'),
    );
  }
}

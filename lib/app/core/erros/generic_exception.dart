

import 'package:exemplo_1_contador_mobx/app/core/erros/generic_error.dart';

class GenericException extends GenericError {
  final String? message;
  GenericException({this.message});
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopping.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ShoppingController on _ShoppingControllerBase, Store {
  late final _$itemsAtom =
      Atom(name: '_ShoppingControllerBase.items', context: context);

  @override
  List<ItemEnteties> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(List<ItemEnteties> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  late final _$_ShoppingControllerBaseActionController =
      ActionController(name: '_ShoppingControllerBase', context: context);

  @override
  void addItem(ItemEnteties item) {
    final _$actionInfo = _$_ShoppingControllerBaseActionController.startAction(
        name: '_ShoppingControllerBase.addItem');
    try {
      return super.addItem(item);
    } finally {
      _$_ShoppingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeItem(int itemId) {
    final _$actionInfo = _$_ShoppingControllerBaseActionController.startAction(
        name: '_ShoppingControllerBase.removeItem');
    try {
      return super.removeItem(itemId);
    } finally {
      _$_ShoppingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
items: ${items}
    ''';
  }
}

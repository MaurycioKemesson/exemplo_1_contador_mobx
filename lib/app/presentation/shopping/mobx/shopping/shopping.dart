
import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';
import 'package:mobx/mobx.dart';
part 'shopping.g.dart';

class ShoppingController = _ShoppingControllerBase with _$ShoppingController;

abstract class _ShoppingControllerBase with Store {
  @observable
  List<ItemEnteties> items = ObservableList<ItemEnteties>();

  @action
  void addItem(ItemEnteties item) => items.add(item);

  @action
  void removeItem(int itemId) => items.removeAt(itemId);
}

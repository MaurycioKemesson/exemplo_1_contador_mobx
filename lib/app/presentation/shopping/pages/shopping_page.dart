import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';
import 'package:exemplo_1_contador_mobx/app/presentation/shopping/mobx/shopping/shopping.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class ShoppingPage extends StatefulWidget {
  const ShoppingPage({Key? key}) : super(key: key);

  @override
  State<ShoppingPage> createState() => _ShoppingPageState();
}

class _ShoppingPageState extends State<ShoppingPage> {
  final ShoppingController shoppingController = ShoppingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Exemplo 2 Lista de compras mobx'),
      ),
      body: Observer(
        builder: (context) => Container(
          child: shoppingController.items.isNotEmpty
              ? ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: shoppingController.items.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text(
                        shoppingController.items[index].name,
                      ),
                    );
                  },
                )
              : const Center(
                  child: Text('Nenhum item adicionado a lista'),
                ),
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FloatingActionButton(
            heroTag: const Text("btn4"),
            onPressed: () => shoppingController.removeItem(0),
            tooltip: 'Remover item',
            child: const Icon(Icons.remove),
          ),
          const SizedBox(
            width: 10,
          ),
          FloatingActionButton(
            heroTag: const Text("btn3"),
            onPressed: () =>
                shoppingController.addItem(ItemEnteties(name: 'Item adicionado')),
            tooltip: 'Adicionar item',
            child: const Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}

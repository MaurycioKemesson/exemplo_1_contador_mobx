import 'package:mobx/mobx.dart';

part 'counter.g.dart';

class CounterController = _CounterControllerBase with _$CounterController;

abstract class _CounterControllerBase with Store {
  @observable
  int count = 0;

  @action
  void add() {
    count++;
  }

  @action
  void sub() {
    count--;
  }
}
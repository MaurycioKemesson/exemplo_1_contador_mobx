
import 'package:dartz/dartz.dart';
import 'package:exemplo_1_contador_mobx/app/core/erros/generic_error.dart';
import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';


abstract class ShoppingUseCase {
  Future<Either<GenericError, List<ItemEnteties>>> getList();
}
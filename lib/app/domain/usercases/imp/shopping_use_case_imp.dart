import 'package:dartz/dartz.dart';
import 'package:exemplo_1_contador_mobx/app/core/erros/generic_error.dart';
import 'package:exemplo_1_contador_mobx/app/domain/entities/item_enteties.dart';
import 'package:exemplo_1_contador_mobx/app/domain/repositories/shopping_repository.dart';
import 'package:exemplo_1_contador_mobx/app/domain/usercases/shopping_use_case.dart';

class ShoppingUseCaseImp extends ShoppingUseCase {
  ShoppingUseCaseImp(this._shoppingRepository);

  final ShoppingRepository _shoppingRepository;

  @override
  Future<Either<GenericError, List<ItemEnteties>>> getList() async {
    return await _shoppingRepository.getList();
  }
}

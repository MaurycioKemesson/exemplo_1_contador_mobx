import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:exemplo_1_contador_mobx/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets(
    'Após 5 cliques no botão "Mais" devem aparecer 5 item na tela',
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      await tester.tap(find.byType(ElevatedButton));
      await tester.pumpAndSettle();
      await Future.delayed(const Duration(seconds: 1));

      expect(find.text('Nenhum item adicionado a lista'), findsOneWidget);

      for (var i = 0; i < 5; i++) {
        await tester.tap(find.byIcon(Icons.add));
        await Future.delayed(const Duration(seconds: 1));
      }

      await tester.pumpAndSettle();

      expect(find.text('Item adicionado'), findsNWidgets(5));
    },
  );

  testWidgets(
    'Após 5 cliques no botão "Mais" e 3 cliques no botão "Menos" devem aparecer 2 item na tela',
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      await tester.tap(find.byType(ElevatedButton));
      await tester.pumpAndSettle();
      await Future.delayed(const Duration(seconds: 1));

      expect(find.text('Nenhum item adicionado a lista'), findsOneWidget);

      for (var i = 0; i < 5; i++) {
        await tester.tap(find.byIcon(Icons.add));
        await Future.delayed(const Duration(seconds: 1));
      }
      for (var i = 0; i < 3; i++) {
        await tester.tap(find.byIcon(Icons.remove));
        await Future.delayed(const Duration(seconds: 1));
      }

      await tester.pumpAndSettle();

      expect(find.text('Item adicionado'), findsNWidgets(2));
    },
  );

  testWidgets(
    'Após 5 cliques no botão "Menos" e 1 clique no botão "Mais" deve aparecer 1 item na tela',
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      await tester.tap(find.byType(ElevatedButton));
      await tester.pumpAndSettle();
      await Future.delayed(const Duration(seconds: 1));

      expect(find.text('Nenhum item adicionado a lista'), findsOneWidget);

      for (var i = 0; i < 5; i++) {
        await tester.tap(find.byIcon(Icons.remove));
        await Future.delayed(const Duration(seconds: 1));
      }

      await tester.tap(find.byIcon(Icons.add));
      await Future.delayed(const Duration(seconds: 1));

      await tester.pumpAndSettle();

      expect(find.text('Item adicionado'), findsOneWidget);
    },
  );
}

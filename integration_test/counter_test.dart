import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:exemplo_1_contador_mobx/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets(
    'Após 5 cliques no botão "Mais" o contador deve mostrar 5',
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      for (var i = 0; i < 5; i++) {
        await tester.tap(find.byIcon(Icons.add));
        await Future.delayed(const Duration(seconds: 1));
      }

      await tester.pumpAndSettle();

      expect(find.text('5'), findsOneWidget);
    },
  );

  testWidgets(
    'Após 3 cliques no botão "Mais" e 2 no botão "Menos" o contador deve mostrar 1',
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      for (var i = 0; i < 3; i++) {
        await tester.tap(find.byIcon(Icons.add));
        await Future.delayed(const Duration(seconds: 1));
      }
      for (var i = 0; i < 2; i++) {
        await tester.tap(find.byIcon(Icons.remove));
        await Future.delayed(const Duration(seconds: 1));
      }

      await tester.pumpAndSettle();

      expect(find.text('1'), findsOneWidget);
    },
  );

  testWidgets(
    'Após 5 cliques no botão "Menos" o contador deve mostrar 0',
    (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      for (var i = 0; i < 5; i++) {
        await tester.tap(find.byIcon(Icons.remove));
        await Future.delayed(const Duration(seconds: 1));
      }

      await tester.pumpAndSettle();

      expect(find.text('0'), findsOneWidget);
    },
  );
}
